**Chromium Embedded Framework**

Download latest packages from [CEF releases](http://opensource.spotify.com/cefbuilds/index.html).
    Get the minimal package for Linux x64 and Windows x86&x64
    Get the release symbols for windows.
    Unzip and place them in their corresponding folder within source.

Then push the new build making sure there are no significant changes to the build process.
